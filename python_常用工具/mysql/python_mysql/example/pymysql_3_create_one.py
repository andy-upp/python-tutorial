#! /usr/bin/python
# -*- coding: UTF-8 -*-


from pymysql_comm import UsingMysql


def select_one(cursor):
    cursor.execute("select * from AIops_2018")
    data = cursor.fetchone()
    print("-- 单条记录: {0} ".format(data))


# 新增单条记录
def create_one():

    with UsingMysql(log_time=True) as um:
        sql = "insert into AIOps_2018(uuid, ts, createtime, value, label) values(%s, %s, %s, %s, %s)"
        params = ('1493928180', '1493928180', '2017-05-05 04:03:00','0.2','1')
        um.cursor.execute(sql, params)

        # 查看结果
        select_one(um.cursor)

if __name__ == '__main__':
    create_one()

