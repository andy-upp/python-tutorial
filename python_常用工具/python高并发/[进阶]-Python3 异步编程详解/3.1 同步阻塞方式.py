import socket

#blocking_way() 的作用是建立 socket 连接，发送HTTP请求，然后从
# socket读取HTTP响应并返回数据。
def blocking_way():
    sock = socket.socket()
    #blocking 该调用的作用是向example.com主机的80端口发起网络连接请求
    sock.connect(('example.com',80))
    request = 'GET/HTTP/1.0\r\nHost: example.com\r\n\r\n'
    sock.send(request.encode('ascii'))
    response = b''
    #从socket上读取4K字节数据
    chunk = sock.recv(4096)
    while chunk:
        response += chunk
        #blocking
        chunk = sock.recv(4096)
    return response

def sync_way():
    res = []
    for i in range(10):
        res.append(blocking_way())
        print (res)
    return len(res)

if __name__ == "__main__":
    print (sync_way())