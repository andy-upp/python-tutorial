import socket
from concurrent import futures

#blocking_way() 的作用是建立 socket 连接，发送HTTP请求，然后从
# socket读取HTTP响应并返回数据。
def blocking_way():
    sock = socket.socket()
    #blocking 该调用的作用是向example.com主机的80端口发起网络连接请求
    sock.connect(('example.com',80))
    request = 'GET/HTTP/1.0\r\nHost: example.com\r\n\r\n'
    sock.send(request.encode('ascii'))
    response = b''
    #从socket上读取4K字节数据
    chunk = sock.recv(4096)
    while chunk:
        response += chunk
        #blocking
        chunk = sock.recv(4096)
    return response

#开10个一样的程序同时执行不就行了,于是我们想到了多进程编程
def process_way():
    workers = 10
    with futures.ProcessPoolExecutor(workers) as executor:
        futs = {executor.submit(blocking_way) for i in range(10)}
    return len([fut.result() for fut in futs])

if __name__ == "__main__":
    print (process_way())