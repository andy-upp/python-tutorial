import time
import asyncio

'''
async def 用来定义异步函数，其内部有异步操作。
每个线程有一个事件循环，主线程调用asyncio.get_event_loop()时会创建事件循环，把异步的任务丢给这个循环的run_until_complete()方法，
事件循环会安排协同程序的执行。
上述程序中，hello()会首先打印出Hello world!，
然后，yield from语法可以让我们方便地调用另一个generator。由于await asyncio.sleep(1)也是一个coroutine，所以线程不会等待
asyncio.sleep(1)，而是直接中断并执行下一个消息循环。当asyncio.sleep(1)返回时，线程就可以从yield from拿到返回值（此处是None），
然后接着执行下一行语句。
把asyncio.sleep(1)看成是一个耗时1秒的IO操作，在此期间，主线程并未等待，而是去执行EventLoop中其他可以执行的coroutine了，
因此可以实现并发执行。

'''

#定义async异步函数，中间可以添加await async.sleep(N) 来设定中断并执行下一个循环消息
async def hello():
    print('Hello World:%s' % time.time())
    #必须使用await，不能使用yield from；如果是使用yield from ，需要采用@asyncio.coroutine相对应
    await asyncio.sleep(2)
    print('Hello wow World:%s' % time.time())

def run():
    tasks = [] #任务是对协程进一步封装，其中包含任务的各种状态。即多个coroutine函数可以封装成一组Task然后并发执行
    for i in range(5):
        tasks.append(hello())
    loop.run_until_complete(asyncio.wait(tasks)) #通过事件循环，去调用协程函数

loop = asyncio.get_event_loop() #获取“事件循环”对象

if __name__ =='__main__':
    run()