import asyncio
import aiohttp
import blog_spider

#定义一个协程，在超级循环里可以跑函数
async def async_craw(url):
    print("craw url: ", url)
    #定义一个异步对象，前面需要加async开头
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            #await的时候，超级循环不会等待，直接切换到下一个循环爬取
            result = await resp.text()
            print(f"craw url: {url}, {len(result)}")

#获取超级循环
loop = asyncio.get_event_loop()

#创建task列表
tasks = [
    loop.create_task(async_craw(url))
    for url in blog_spider.urls]

import time

start = time.time()
#执行task，等待它们的完成
loop.run_until_complete(asyncio.wait(tasks))
end = time.time()
print("use time seconds: ", end - start)
