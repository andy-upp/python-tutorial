import flask
from concurrent.futures import ProcessPoolExecutor
import math
import json

#使用进程池加速

app = flask.Flask(__name__)


def is_prime(n):
    if n < 2:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    sqrt_n = int(math.floor(math.sqrt(n)))
    for i in range(3, sqrt_n + 1, 2):
        if n % i == 0:
            return False
    return True


@app.route("/is_prime/<numbers>")
def api_is_prime(numbers):
    number_list = [int(x) for x in numbers.split(",")]
    results = process_pool.map(is_prime, number_list)
    return json.dumps(dict(zip(number_list, results)))


if __name__ == "__main__":
    #当我们定义pool和时候，它所依赖的函数必须都已经声明完成了
    #所以 ProcessPoolExecutor() 必须放在函数声明的最下面才能使用
    #且定义再 main 函数中
    process_pool = ProcessPoolExecutor()
    app.run()
