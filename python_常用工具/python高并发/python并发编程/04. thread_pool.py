import concurrent.futures #引入线程池模块
import blog_spider

# craw
with concurrent.futures.ThreadPoolExecutor() as pool:
    #创建线程池的第一种方式，pool.map()
    #第一个参数是一个函数，第二个参数是一个url列表
    htmls = pool.map(blog_spider.craw, blog_spider.urls)
    #将每一个url及其对应的html拼接起来
    htmls = list(zip(blog_spider.urls, htmls))
    for url, html in htmls:
        print(url, len(html))

print("craw over")

# parse
with concurrent.futures.ThreadPoolExecutor() as pool:
    futures = {}
    for url, html in htmls:
        #使用submit的方式创建线程池
        #第一个参数是函数，第二个参数是一个html，submit 需要挨个提交
        #返回值是一个future对象
        future = pool.submit(blog_spider.parse, html)
        futures[future] = url

    #按顺序打印执行结果
    #for future, url in futures.items():
    #    print(url, future.result())

    #as_completed哪个任务先执行完成，就率先返回哪个任务
    for future in concurrent.futures.as_completed(futures):
        url = futures[future]
        print(url, future.result())