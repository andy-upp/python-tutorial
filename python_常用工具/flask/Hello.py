from flask import Flask
import logging
import datetime

app = Flask(__name__)
@app.route('/')
def hello_world():
   rt = "发生异常"
   try:
      rt = "bbb"
      y = m

   except Exception as e:
      print ("bbbb")
      #app.logger.debug(e);
      with open("./logs/logs.log", "a", encoding="utf-8") as f:
         f.write(str(datetime.datetime.now()),)
         f.write(" : ")
         f.write(str(e))  # 追加模式
         f.write("\n")
      # with open("test.txt", "w", encoding="utf-8") as f:
      #    f.write("你曾经对我说\n")  # 文件不存在则创建一个，如果文件存在，新写入的内容会覆盖掉原内容，一定要注意
      #    f.write("你永远爱着我\n")  # 如需要换行，末尾添加换行符 \n
   return rt


if __name__ == '__main__':
   app.debug = True
   handler = logging.FileHandler('./logs/flask.log')
   app.logger.addHandler(handler)
   app.run()
