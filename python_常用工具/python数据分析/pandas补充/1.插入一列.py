import pandas as pd
import numpy as np
data = {
    'school' : ['北京大学', '清华大学', '山西大学', '山西大学', '武汉大学'],
    'name' : ['江路离', '沈希梦', '来使鹭', '陈曦冉', '姜浩然'],
    'No.' : [20001943, 300044451, 20190006, 20191234, 1242522]
}
# data = list(data)  <-> data = list(data.keys)
# data = list(data.values())

frame = pd.DataFrame(data)

frame.insert(0, 'num', np.ones(5))
print(frame)

list_ = [1,2,3,4,5]
nplsit = np.array(list_)

frame.insert(0, 'num1', nplsit )
frame['num2'] = list_

print(frame)

# print(frame['name'].sum())
# frame.to_csv("./test.csv",index=None) #不保留行索引

print(frame['No.'].tolist())

#
# L = ['Adam', 'Lisa', 'Bart']
# L[0]='Bart'
# L[-1]='Adam'
# print (L)



numbers = [1,7,3,2,5,6,2,3,4,1,5]
new_numbers = list(set(numbers))
print(new_numbers)

df = pd.DataFrame([[0, 2, 3], [0, 4, 1], [10, 20, 30]], columns=['A', 'B', 'C'])
print(df)
print(df.at[2, 'B'])