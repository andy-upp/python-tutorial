import numpy as np
import matplotlib.pyplot as plt



size = 7
x = np.arange(size)
print(x)
plt.figure(figsize=(40,40))

# x = ["ss","ss","ss","ss","ss"]
# a = np.random.random(size)
# b = np.random.random(size)
# c = np.random.random(size)
#
# print (a)
xticks = ['AB', 'PC', 'MC', "OC", "LO", "KN", "MA"]


a = [75, 75, 76, 72, 76, 72, 76]
b = [33, 32, 15, 20, 56, 72, 76]
c = [33, 30, 10, 21, 34, 72, 76]

total_width, n = 0.8, 3
width = total_width / n
x = x - (total_width - width) / 2


for i in range(28):
    plt.subplot(4,7,i+1) #要生成两行两列，这是第一个图plt.subplot('行','列','编号')
    plt.xticks(np.arange(len(xticks)) + 0.4 / 2, xticks)
    plt.bar(x, a, width=width, label='a')
    plt.bar(x + width, b, width=width, label='b')
    plt.bar(x + 2 * width, c, width=width, label='c')
    plt.legend()
# plt.show()
plt.savefig("2018_10methods.png")

