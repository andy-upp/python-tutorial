import numpy as np
import matplotlib.pyplot as plt


y1 = [75, 75, 76, 72, 76, 73, 72, 97]
y2 = [33, 32, 15, 20, 56, 21, 20, 0]

xticks = ['ABOD', 'PCA', 'MCD', "OCSVM", "LOF", "KNN", "MAD", "statistic"]


def bar():
    plt.title('effect comparison', fontsize=6)
    plt.bar(np.arange(len(y1)), y1, width=0.4, color='tomato', label='underreporting rate')
    for x, y in enumerate(y1):
        plt.text(x, y, y)

    plt.bar(np.arange(len(y2)) + 0.4, y2, width=0.4, color='steelblue', label='false positive rate')
    for x, y in enumerate(y2):
        plt.text(x + 0.4, y, y)

    plt.xticks(np.arange(len(xticks)) + 0.4 / 2, xticks)
    plt.ylabel('percentage%')
    plt.legend(loc='upper left')
    plt.show()


bar()