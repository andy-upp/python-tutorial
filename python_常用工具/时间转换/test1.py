from datetime import datetime, timedelta

#格林尼治时间转标准时间
def india_to_local(india_time_str,india_format = '%Y-%m-%dT%H:%M:%S+08:00'):
        india_dt = datetime.strptime(india_time_str, india_format)
        local_dt = india_dt + timedelta(hours=2, minutes=30)
        local_format = "%Y-%m-%d %H:%M:%S"
        time_str = local_dt.strftime(local_format)
        return time_str
a = india_to_local("2021-09-16T10:32:00+08:00")
print(a)