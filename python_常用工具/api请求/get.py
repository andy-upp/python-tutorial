import requests
import json
import time

headers = {
    'Authorization':'MDcxZjViM2FmMzg2OTRhYjIzMDVlMWU0NDQ1OWE5ZTIzYTUxYjU4Ng=='
}

url = 'http://eas-zhangbei-b.alibaba-inc.com/api/predict/interface_service'

data = "[{\"gameId\":\"10000100\",\"content\":\"哈哈哈  这句话 哈哈哈哈。\"}]"


ticks = time.time()
print ("当前时间戳为:", ticks)
for i in range(100):
    response = requests.post(url, headers=headers, data=data.encode('utf-8'))
    # print(response)
    print(response.content)


ticks = time.time()
print ("当前时间戳为:", ticks)

# print(type(int(json.loads(response.content)["result"])))  # 取出响应内容中 "result" 字段的值
# print(int(json.loads(response.content)["result"], 16))  # 16 进制字符串转成整型 int
# print(type(int(json.loads(response.content)["result"], 16)))

### 注：
### 在 Linux 下需要对 response.content进行解码，将其从 bytes 类型转为 str 类型
### 具体操作：
### bytes.decode(response.content) 或者 response.content.decode()
