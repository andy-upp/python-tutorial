#问题 1：python 中 yield 的用法

'''
yield表达式主要用在生成器函数和异步生成器函数中：

在一个函数体内使用yield表达式会使这个函数返回一个生成器。在调用该函数的时候不会执行该函数，而是返回一个生成器对象。
对这个对象执行for循环时，每次循环都会执行该函数内部的代码，执行到yield时，该函数就返回一个迭代值，下次迭代时，
代码从yield的下一条语句继续执行，而函数的本地变量看起来和上次中断执行前是完全一样的，
于是函数继续执行，直到再次遇到yield。
在一个async def定义的函数体内使用yield表达式会让协程函数变成异步的生成器。
'''

def gen():  # defines a generator function
    yield 123

async def agen(): # defines an asynchronous generator function
    yield 123


    