#问题：python2 和 python3 有哪些区别
'''
print
print 语句没有了，取而代之的是 print () 函数。

Unicode
Python2 有 ASCII str () 类型，unicode () 是单独的，不是 byte 类型。
在 Python 3，我们最终有了 Unicode (utf-8) 字符串，以及一个字节类：byte 和 bytearrays。

除法运算
Python2 中的整数相除的结果是一个整数，比如 1 / 2 得到的是 0。
Python3 中的整数相除的结果是一个浮点数，如果你想得到一个整数，可以使用 floor 除法，像这样 1 // 2。

xrange
Python2 中，用 xrange 返回类似于迭代器的对象，这要比 range 要快很多，它是惰性求值，而不是一次性求值。
Python3 已经去掉了 xrange，因为 Python3 的 range 就是 Python2 的 xrange，所以就没有再保留 xrange。

不等运算符
Python 2.x 中不等于有两种写法！= 和 <>。
Python 3.x 中去掉了 <>, 只有！= 一种写法。

repr 表达式
Python 2.x 中反引号 ` ` 相当于 repr 函数的作用。
Python 3.x 中去掉了 ` ` 这种写法，只允许使用 repr 函数。

long 类型
Python 3.x 去除了 long 类型，现在只有一种整型 int。

高阶函数
Python 3.x 高阶函数 map、filter、zip 返回的是迭代器，不是列表对象了，而 reduce 也不再是内建函数了。

'''

#问题：python 中的异常处理，写一个简单的应用场景


def AbyB(a, b):
    try:
        c = ((a+b) / (a-b))
    except ZeroDivisionError:
        print "a/b result in 0"
    else:
        print c


#try-except 完整版的格式：

try:
    do_something # 这个执行过程可能会raise错误
except Exception:
    handle_exception_error # 捕获到异常后的处理
else:
    handle_non_exception # 未捕获到异常后的处理
finally:
    cleanup # 无论异常发生与否，都会执行这部分


