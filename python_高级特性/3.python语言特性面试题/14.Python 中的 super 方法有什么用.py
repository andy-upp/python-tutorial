#问题：Python 的 super 方法有什么用？

'''
在子类中使用super函数，它会在当前实例的继承树中（MRO列表）搜索下一个基类，把基类方法绑定到当前实例上并调用。
'''

class F:
    def foo(self):
        print("I'm F.foo")

class C(F):
    def foo(self):
        # 子类如果重写父类方法foo，会直接覆盖父类方法
        # 如果我想在这里执行父类的foo方法，该怎么办？
        super().foo() # 《====== 答案：调用super().foo()，
        print("I'm C.foo")

c = C()
c.foo()

#问题：super函数的使用场景：
'''
1.单继承
super可用来引用父类而不必显式地指定它们的名称，从而令代码更易维护，向前兼容性。
'''
class Parent(object):
    def __init__(self):
        pass

class Child(Parent): # 1. 继承父类Parent
    def __init__(self):        # 2.调用super去初始化基类。
        # 3没有指明父类的名称，如果需要修改父类，只需要在#1修改，无需在每个引用到父类方法的地方去修改。
        super().__init__() 
        # 4.可以再对子类的其它特有的属性初始化或者覆盖某些父类属性。这样就可以既继承了父类的功能, 又要了新的功能。
        self._other_init()

'''
2.多重继承

多重继承是Python所独有的一种特性，这时会有多个基类实现相同的方法，
这就有可能可以实现“菱形图”(“diamond problem”)。
'''

class Parent(object):
    def __init__(self):
        print("enter Parent")
        print("leave Parent")

class ChildA(Parent):
    def __init__(self):
        print("enter ChildA")
        Parent.__init__(self) # TODO: 替换成 super().__init__()
        print("leave ChildA")

class ChildB(Parent):
    def __init__(self):
        print("enter ChildB")
        Parent.__init__(self) # TODO: 替换成 super().__init__()
        print("leave ChildB")

class GrandChild(ChildA, ChildB):
    def __init__(self):
        print("enter GrandChild")
        ChildA.__init__(self) # TODO: 替换成 super().__init__()
        ChildB.__init__(self) # TODO: 去掉这行
        print("leave GrandChild")