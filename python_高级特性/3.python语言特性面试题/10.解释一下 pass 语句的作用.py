#问题：解释一下 pass 语句的作用
'''
pass是一个空操作。当它被执行时，什么都不发生。
它适合当语法上需要一条语句但并不需要执行任何代码时用来临时占位。
'''

print ("字符串中有单引号，可以使用双引号或三引号创建")

s = "aaaaa'bbbbb'ccccc"

print ("字符串中有双引号，可以使用单引号或三引号创建")

s = 'aaaaa"bbbbb"ccccc'

print ("字符串是多行的，可以使用三引号创建")

s = """
There are three thousand things in this world.
Sun,Moon,and you.
The sun is morning.
The moon is night.
You are  forever.
"""