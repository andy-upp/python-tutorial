#什么是自省函数，常见的自省函数有哪些
'''
自省就是能够在运行时确定对象的类型或属性的能力，获取它们的信息，并对它们进行操作。 
有了自省，我们能够定义没有名称的函数，不按函数声明的参数顺序调用函数，
甚至引用事先并不知道名称的函数

常见的自省函数有:
dir()：返回与该对象关联的方法和属性的列表。
help()：启动内置的帮助系统，查看很多 Python 自带的帮助文档信息。
id()：返回对象的 “标识值”。
type()：返回对象的类型。
hasattr() 和 getattr()：分别判断对象是否有某个属性及获得某个属性值。
callable()：如果实参是可调用的，返回 True，否则返回 False。
isinstance()：可以确认某个对象是属于某种类型。

'''
alist=[]
print (dir(alist))

#构造一个方法对同一类型的对象按照某个属性进行排序
class Person(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __repr__(self):
        # 用于显示Person对象
        return "Person name:{}, age: {}".format(self.name, self.age)

def persons_sort_by(persons, key):
    # persons是一个Person对象列表
    return sorted(persons, key=lambda p: getattr(p, key))

p1 = Person(name='John', age=20)
p2 = Person(name='Alex', age=24)
p3 = Person(name='Young', age=15)
print (persons_sort_by([p1, p2, p3], key='name'))