#问题 1：__new__和__init__的区别

 '''
 __new__:

是静态方法
用于创建实例
第一个参数是类型并且（通常）返回该类型的新实例

__init__:

是实例方法
用于初始化实例
第一个参数是实例本身，不返回任何东西
 '''

#问题 2：简述 with 方法打开处理文件帮我们做了什么？
'''
with 语句给需要处理的文件创建了一个上下文管理器对象，当 with 控制块运行完了，文件会自动关闭。
'''



#问题 3: 如何在 python 中读取大文件

'''
方法 1 直接利用 with 语句的特性，上下文管理对象去处理打开和关闭文件，包括如果内部块中引发异常。
for 行将文件对象 file 视为可迭代，它会自动使用缓冲的 I/O 和内存管理，因此您不必担心大型文件。
'''
with open('filename') as file:
  for line in file:
    do_something(line)

'''
方法 2 则是自己手动去写一个生成器函数去返回一个迭代器，
遍历一次就输出一个文件数据块，这样也不会一次把内存都占满。
但是麻烦的是，你需要手动关闭文件。
'''

def readInChunks(fileObj, chunkSize=2048):
      while True:
    data = fileObj.read(chunkSize)

    if not data:
      break

    yield data

