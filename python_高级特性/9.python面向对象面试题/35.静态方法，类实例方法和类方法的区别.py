#问题：静态方法，类实例方法和类方法的区别

'''
答案
类方法
是类对象的方法，在定义时需要在上方使用@classmethod进行装饰，形参为cls，表示类对象，类对象和实例对象都可调用。

类实例方法
是类实例化对象的方法，只有实例对象可以调用，形参为self，指代对象本身。

静态方法
是一个任意函数，在其上方使用@staticmethod进行装饰，可以用对象直接调用。

'''

class Demo(object):
    def method(*argv):
        return argv

    @classmethod
    def klassmethod(*argv):
        return argv

    @staticmethod
    def statmethod(*argv):
        return argv

print (Demo().method)
print (Demo().method()) #自动把实例对象看做method的第一个参数
print (Demo().statmethod)
print (Demo().statmethod())