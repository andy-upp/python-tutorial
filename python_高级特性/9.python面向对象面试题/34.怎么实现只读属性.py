#问题：怎么实现只读属性？

'''
使用 @property 定义只读属性。
'''

class MyClass:
    def __init__(self):
        self._x = None
        self._y = 1

    def getx(self):
        return self._x

    def setx(self, value):
        self._x = value

    def delx(self):
        del self._x

    def gety(self):
        return self._y

    x = property(getx, setx, delx, "I'm the 'x' property.")
    y = property(gety, doc="I'm the 'y' property.")


c = MyClass()
c.x = 3
print (c.x)


class MyClass1:
    def __init__(self):
        self._x = None
        self._y = 1

    @property
    def x(self): # x是一个公有（可读可写）属性
        """
        I'm the 'x' property.
        """
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @x.deleter
    def x(self):
        del self._x

    @property
    def y(self): # 只设置getter，y是一个私有（只读）属性
        """
        I'm the 'y' property.
        """
        return self._y


