#问题：字典操作中del和pop有什么区别
'''
del 和 pop 的区别：

del ：使用语法 del dic["k"] ，移除字典中键为 k 的键值对，没有返回值，
但是如果字典中没有 k 这个键，会抛出异常 keyError。
pop：使用语法 dic.pop(k, [default]) ,返回键 k 所对应的值，然后移除这个键值对。
如果没有这个键，返回指定的 default 值。如果 default 未给出且 key 不存在于字典中，
则会引发 KeyError 。
'''

dic = {"a":"b","c":"d"}
#del dic["e"] # KeyError: 'e' 

return_value = dic.pop("a",[default])
#print(return_value) # 打印 return_value 

#问题 字典的常用操作有哪些
'''

len(d) 返回键-值对的长度

d[k] 返回键k上的值

d[k] = v 把键k上的值设为v

del d[k] 删除键为k的元素

k in d 检查d中是否包含键为k的项

d.clear() 移除所有元素

d.copy() 浅复制

d.get(k, [default]) 返回键k上的值，如果没有，返回default值。如果 default未给出则默认为None，因而此方法绝不会引发KeyError。

d.popitem()随机返回一个键值并从字典里移除它

d.update(m)把新字典m的键值对更新到d中，返回None

d.items() 返回所有的键值对
d.keys() 返回所有的键
d.values() 返回所有的值

'''

#处理异常

try:
    del d["a"]
except KeyError:
    print ("key a not found")