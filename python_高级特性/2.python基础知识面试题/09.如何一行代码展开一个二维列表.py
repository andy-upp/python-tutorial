#问题：如何一行代码展开一个二维列表
#问题：[[1,2],[3,包],[5,6]]一行代码展开该列表，得出[1,2,3,包,5,6]

from functools import reduce
print (reduce(lambda x, y: x + y, [[1,2],[3,4],[5,6]]))

#问题：简述any()和all()方法
'''
any(iterable)表示如果传入的可迭代对象有一个为True，这个函数就返回True。
而all(iterable)表示如果传入的可迭代对象所有的元素都为True，这个函数才返回True。
'''

#问题：简述高阶函数的用法

'''
高阶函数的定义是，即满足以下一个条件的函数：

  接受一个或多个函数作为输入
  输出一个函数

  在Python里，reduce，map，filter是常用的三个高阶函数。
'''


#问题：用高阶函数实现列表元素求和

reduce(lambda x, y: x + y, [1,2,3])