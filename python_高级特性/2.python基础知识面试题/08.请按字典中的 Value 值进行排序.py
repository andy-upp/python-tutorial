#问题：请按字典中的 Value 值进行排序

'''
使用Python内置排序函数sorted
'''
dict(sorted(d.items(), key=lambda x: x[1])) # 降序
dict(sorted(d.items(), key=lambda x: x[1], reverse=True)) # 升序

#问题：python 的常用排序方案

'''
Python的常用排序方案：有两种 sort() 函数和sorted() 函数。
  列表方法list.sort()修改列表本身。
  内置函数sorted()可以从一个迭代对象构建返回一个新的排序列表。

'''