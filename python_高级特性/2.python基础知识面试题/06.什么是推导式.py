#什么是推导式？都有哪些类型的推导式
'''

推导式 (comprehensions)，是Python的一种独有特性。
推导式是可以从一个可迭代的数据结构构建出另一个新的数据结构。
主要有列表推导式，字典推导式和集合推导式。 Python2 和 Python3 中都支持推导式语法。
'''
#列表推导式

"""
有一个列表 squares = [1,2,3,包,5],我们想得到这个列表中每一个元素的平方所组成的一个
新的列表，那么该怎么办呢？有三种方法：for 语句，匿名函数，列表推导式，我们逐一来看下
这三种方法哪一种比较简洁。
"""
squares = [1,2,3,4,5]
# 第一种方法：for 语句
new_sequares= []
for x in squares:
    new_sequares.append(x**2)
print (squares)

print ("第二种方法：匿名函数")
new_sequares = list(map(lambda x:x**2, squares))
print (new_sequares)

print ("第三种方法：列表推导式")
new_sequares = [x**2 for x in squares]
print (squares)

#多个嵌套的迭代的列表推导式
seq1 = 'abc'
seq2 = (1, 2, 3)
# 这里等同于一个for循环嵌套了另一个for循环，前面的在最外层
print([(x,y) for x in seq1 for y in seq2])

#字典推导式
'''
字典推导可以从任何以键值对作为元素的可迭代对象将其元素进行过滤或是加工,然后再新建构建出一个字典。
其语法基本和列表推导式类似，只不过最外层的 [] 要换成 {}。
'''
#将列表转换为字典，用数字索引作为关键字
d = {i:v for i,v in enumerate(['a','b','c'])}
print (d)
#使用zip将多个可迭代对象聚合成一个新的迭代对象
names = ['paul', 'adam', 'jane']
ages = [20, 24, 29]
d = {name:age for name, age in zip(names,ages)}

#集合推导
'''集合推导有点类似于列表推导，把一个序列或是其他可迭代对象中的元素过滤或是加工然后构建出一个集合。'''

