#求两个列表的交集、差集、并集


'''
答案
求交集: list(set(a) & set(b))

求差集: list(set(a) - set(b))

求并集: list(set(a) | set(b))

'''