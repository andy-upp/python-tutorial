#问题：hasattr() getattr() setattr()的用法

'''
hasattr(object, name)：判断一个对象里面是否有 name 属性或者 name 方法，
返回布尔值，有name特性返回True， 否则返回False。

getattr(object, name[,default])：获取对象 object 的属性或者方法，
如果存在返回该属性值，如果不存在，返回默认值。默认值可选。如果没有设置默认值，
且属性不存在会抛出AttributieError。

setattr(object, name, values)：给对象的属性赋值，
若属性不存在，先创建再赋值。

'''

#问题：问题：如何实现Python函数重载
from functools import singledispatch

'''
Python 重载机制: 转发（Dispatch），
即通过使用单分派函数functools.singledispatch来实现函数重载。
'''

@singledispatch
def to_str(obj):
    print('%r'%(obj))

# to_str是上面标记的基函数，函数参数类型是作为register的参数传入，从而实现重载
@to_str.register(int)
def _(obj): # 专门函数的名称无关紧要，所以使用`_`这个占位符即可。并且切记不要和基函数名字一样，否则会覆盖。
    print('Integer: %d'%(obj))

@to_str.register(str)
def _(obj):
    print('String: %s'%(obj))

@to_str.register(list)
def _(obj):
    print('List: %r'%(obj))

if __name__ == "__main__":
    to_str(1)
    to_str('hello')
    to_str(range(3))
    to_str(object)









