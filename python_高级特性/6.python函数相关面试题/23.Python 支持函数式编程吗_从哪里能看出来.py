#问题：Python 支持函数式编程吗？为什么？从哪些方面可以看出来？
'''
函数式编程:你的所有操作都可以由函数进行完成，函数和其他数据类型拥有平等的地位，可以赋值给变量，
也可以作为参数传入另一个函数，或者作为别的函数的返回值
Python 支持函数式编程，主要分以下几种：
  Python 的一些语言特性，比如lambda匿名函数、列表推导、字典推导、生成器、迭代器等
  Python 的一些内置函数，包括map、reduce、filter、all、any、enumerate、zip等高阶函数
  Python 的一些内置模块，比如 itertools、functools 和 operator模块等
'''
#example 同时用指令式编程和函数式编程两种方式去实现列表元素加1的功能
# 指令式编程
# 先确立初始状态，然后每次迭代都执行循环体中的一系列命令
newList = []
for num in range(1,10):
    newList.append(num+1)
print (newList)
# 函数式编程
# 以函数的方式，去对数据进行转换，我们可以这样想象这样一个函数f(x) = x + 1
newList = list(map(lambda x:x+1,list(range(1,10))))
print (newList)

#functools.partial()函数

from functools import partial

def log_template(level, message):
    print("{}: {}".format(level, message))

log_info = partial(log_template, "info")
log_warning = partial(log_template, "warning")
log_error = partial(log_template, "error")


#问题：什么是lambda函数？它有什么好处?
'''
lambda表达式被用于创建匿名函数，一般在一些一次性运行的程序中使用。因为不需要定义函数名，
所以不用担心函数名称冲突的问题，而且匿名函数更加的节省内存中的变量定义空间。
'''
#自由变量 只有在运行的时候绑定到函数中，而非定义时绑定
x = 10
func1 = lambda y: x + y
x = 20
func2 = lambda y: x + y
print (func1(10))
print (func2(10))

#如果我想在定义的时候去绑定变量，可以使用函数默认值参数形式
x = 10
func3 = lambda y, x=x: x+ y # x=x这样可以在定义时绑定到值
x = 20
print (func3(10))








