#什么是闭包
'''
在一个内部函数中，对外部作用域的变量进行引用，(并且一般外部函数的返回值为内部函数)，
那么内部函数就被认为是闭包。简而言之，闭包指延伸作用域的函数。
'''

#闭包的两个要素：
#  1.嵌套在另一个函数中的函数   
#  2.自由变量的引用 即便生成闭包的环境（父函数）已经释放，闭包仍然存在
#  绑定的变量不会因为父函数被释放了也随之释放，而是，它可以继续被内部函数继续引用。

def plus_one():
    v = 1
    def add(new_value):
        print (v + new_value)
        return v + new_value

    return add # 这里返回一个函数对象

a = plus_one() #对函数对象的调用，要使用括号
a(1)


#如果想继续引用那个外部变量并且要给它赋值，可以使用nonlocal
def average():
    total = 1
    counter = 0
    def calculate(new_value):
        nonlocal total  # 告诉Python这个变量是自由变量
        nonlocal counter
        total += new_value
        counter += 1
        print (total / counter)
        return total / counter
    return calculate
test = average()
test(1)

