#-*- coding: utf-8 -*-

print ("=============数字类型===========")
#1.1数字类型
#int整形 整数 默认输入十进制 另外二进制是0b，八进制是0o，十六进制是0X
print (2)
print (16==0b10000==0o0==0x10)
#10进制与其他进制的转换,转换后为字符串类型
a = bin(16) #转二进制
b = oct(16) #转八进制
c = hex(16) #转十六机制
print (a,b,c)
#其他进制转十进制
d = int(a,2) #二进制转十进制
e = int(b,8) #八进制转十进制
f = int(c,16) #十六进制转十进制
print (d,e,f)


#浮点型 带小数的数 具有不确定性
#计算机采用二进制小数来表示浮点数的小数部分，部分小数不能用二进制小数完全表示
#通常不会影响计算精度
print (2.5)
print (0.1+0.2)== 0.3
a = 3*0.1
print (a)
#四舍五入获取精确解
a = 0.300000001
b = round(a,1)
print (b == 0.3)

#复数类型 a+bj 大写J或者小写j均可
print (3+4j)
print (2+5J)
#当虚部系数为1时，需要显式写出
print (2+1j)
#数字运算操作符 加减乘除 + - / *
print ((1*3-4*2)/5)
#取反运算 - 乘方运算 ** 整数商 //  模运算 %
#整数与浮点数运算当结果是浮点数  除法运算当结果是浮点数
#数字运算操作函数
#求绝对值
print (abs(-5))
#求幂次方
print (pow(2,5))
#求2的5次幂然后对3求余数
print (pow(2,5,3))
#四舍五入函数 round第一位为需要四舍五入的数，第二位为保留的小数位数，不写则默认为整数
a = 1.618
print (round(a))
#序列的最大最小值,求和
print (max(3,2,1,3,5,2))
print (min(3,2,1,3,5,2))
print (sum([4,3,2,4,5]))
#借助科学计算库
import math
print (math.exp(1)) #指数运算 e^x
print (math.log(8)) #对数运算 ？
print (math.sqrt(4)) #开平方运算
import numpy as np
a = [1,2,3,4,5]
print (np.mean(a)) #求均值
print (np.median(a)) #求中位数
print (np.std(a)) #求标准差

print ("=====================字符串类型===================")

i = 2

print("ssss{}.sss".format(i))


# 1.2字符串类型，由数字 字母 空格或者其它字符组合而成，用 ""或'' 表达
print ("pyhton 124  ")
print  ('pyhton &678')
#单中有双 双中有单
print ("i'm 18 year old")
print ('"Python" is good')
#双中有双，单中有单 -- 转义符 \
print ("\" Python\" is good")
#转义字符可以用来换行
s = "py\
thon "
print (s)
#字符串的索引
#正向索引
s = "My name is Peppa Pig"
print (s[0])
print (s[1])
print (s[2])
#反向索引  从-1开始递减
print (s[-1])
print (s[-2])
print (s[-3])

#字符串的切片  变量名[开始位置：结束位置：切片间隔]
#切片间隔(位置之差)如不设置默认为1，可省略，切片范围不包括结束位置
s = "Python"
print (s[0:4:2])
#起始位置是0可以省略，结束位置省略，代表可以取到最后一个字符，可以使用反向索引
s = "Python"
#以下的输出都是python
print(s[0:6])
print(s[:6])
print(s[:])
print(s[-6:]) #使用反向索引
#反向切片
#起始位置是-1可以省略 结束位置省略，代表可以取到第一个字符
s = "123456789"
#以下输出都是987654321
print(s[-1:-10:-1])
print(s[:-10:-1])
print(s[::-1])

#字符串的拼接
a = "I love "
b = "my wife"
print(a+b)
#字符串的成倍复制  字符串*n  n*字符串
c = a + b
print (c*3)
print (3*c)
#成员运算
#子集in全集  任何一个连续的切片都是原字符串的子集
folk_sigers = "Peter, Paul and Mary"
print ("Peter" in folk_sigers)
print ("PPM" in folk_sigers)
#遍历字符串字符  for 字符 in 字符串
for s in "Python":
    print (s)

#字符串处理函数
#字符串的长度
s = "python"
print (len(s))

#字符编码
#将中文字库，英文字母，数字，特殊字符等编码转化成计算机可识别的二进制数
#每个单一字符对应一个唯一的互不重复的二进制编码，python中使用的是Unicode编码
#将字符转化为 Unicode码 - ord（字符）
print(ord("1"))
print(ord("a"))
print(ord("*"))
#将Unicode码转化为字符  chr(Unicode码)
# print (chr(1010))
# print(chr(10000))
# print(chr(12345))
# print(chr(23456))
#字符串的处理方法
#字符串的分割--字符串.split(分割字符)
#返回一个列表 原字符串不变
languages = "Python C C++ Java PHP R"
languages_list = languages.split(" ")
print(languages_list)
print(languages)
#聚合 "聚合字符".join(可迭代数据类型)
#可迭代数据类型 ：字符串 字符串列表
s = "12345"
s_join = ",".join(s) # 将可迭代对象没相邻的两个元素之间加一个逗号
print (s_join)
s =  ["1","2","3","包","5"]
s_join1 = "*".join(s)
print  (s_join1)
#删除两端特定字符--字符串.strip(删除字符)
#strip从两侧开始搜索，遇到指定字符执行删除，遇到非指定字符，搜索停止
#类似的还有左删除lstrip和rstrip
s = "  I have many blanks  "
print (s.strip(" "))
print (s.lstrip(" "))
print (s.rstrip(" "))
#字符串替换 字符串.replace("被替换"，"替换成")
s = "Python is coming"
sl = s.replace("Python","Py")
print(sl)
#字符串统计 字符串.count("待统计字符串")
s = "Python is an excellent language"
print("an:", s.count("an"))
print("e:",s.count("e"))
#字符串大小写
#字符串.upper() 字母全部大写
s = "Python"
print(s.upper())
#字符串.lower 字母全部小写
print (s.lower())
#字符串.title() 首字母大写
print ("python".title())


print("=============布尔类型==============")


#1.3 布尔类型 主要用于逻辑运算
print (2>1)
print (True)
#any()只要有一个是非零 就是true
#all()所有元素都是非零才是true
print (any([False,1,0,None]))
print (all([False,1,0,None]))
#布尔类型作为指示条件
while True:
    print ("test")
    break
#布尔类型作为掩码
x = np.array([[1,3,2,5,7]])
print (x>3)
print (x[x>3])

#数据类型的判别 type(变量)
age = 20
name = "Ada"
print (type(age))
print (type(name))
# 预判类型) 承认继承
#变量类型是预判类型的字类型，则为真，否则为假
print(isinstance(age,int))  # 判断age是否是int这个类的实例
print(isinstance(age,object)) #object是所有类的父类
print(isinstance(name,object))

#字符串检查方法
#字符串.isdigit()检查字符串是否只有数字组成
#字符串.isalpha()检查字符是否只有字母组成
#字符串.isalnum()检查字符串是否只有字母和数字组成
age = "20"
name = 'Ada'
print (age.isdigit())
print (name.isdigit())
print ("Ada".isalpha())
print ("Ada20".isalnum())
#类型转换
#数字类型转字符串 str(数字类型)
age = 20
print ("My age is "+str(age))
#仅有数字组成的字符串转数字 int(),float(),eval()
s1 = "20"
s2 = "10.1"
print (int(s1))
print (float(s1))
print (float(s2))
#用eval可以直接取出字符串里面的内容
print (eval(s1))














