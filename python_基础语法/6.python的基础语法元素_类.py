#-*- coding: utf-8 -*-

print ("=============类的三要素：类名、属性、方法==============")
#类是对象的载体，我们可以把一类对象的公共特征抽象出来，创建通用的类
#创建类
class Cat():
    """模拟篇"""
    def __init__(self,name):
        """初始化属性"""
        self.name = name
    def jump(self):
        """模拟猫的跳跃"""
        print(self.name+" is jumping")

#用类创建实例
my_cat = Cat("Loser")
your_cat = Cat("Lucky")

#调用属性
print(my_cat.name)
print(your_cat.name)
#调用方法
my_cat.jump()
your_cat.jump()

print("=============类的命名==============")
#类的命名要有实际意义 采用驼峰命名法，即组成的单词首字母大写
#类的属性
#def __init__(self,要传递的参数）  初始化类的操作
#无论是初始化类还是定义方法的时候，都需要有self参数
class Car():
    """模拟汽车"""

    def __init__(self,brand,model,year):
        """初始化汽车属性"""         #相当于类内部的变量
        self.brand = brand
        self.model = model
        self.year = year
        self.mileage = 0   #新车总里程初始化为0

    def get_main_infomation(self):  #self不能省略
        """获取汽车的主要信息"""
        print("品牌：{} 型号：{} 出厂年份：{}".format(self.brand,self.model,self.year))

    def get_mileage(self):
        """获取总里程数"""
        return "汽车行驶总里程：{}公里".format(self.mileage)

print("=============创建实例================")
#创建实例  将实例赋值给对象，实例化过程中，传入相应的参数 v = 类名（必要的初始化参数）
my_new_car = Car("Audi","A6","2018")

#访问属性  类名.类属性
print(my_new_car.brand)
print(my_new_car.model)
print(my_new_car.mileage)
#方法调用  实例名.方法名（必要的参数）
my_new_car = Car("Audi","A6","2018")
my_new_car.get_main_infomation()
s = my_new_car.get_mileage()
print (s)

#属性修改
#直接修改 先访问 后修改
my_new_car = Car("BYD","宋",2016)
my_new_car.mileage = 12000
print(my_new_car.mileage)

#通过方法修改属性
class Car1():
    """模拟汽车"""

    def __init__(self,brand,model,year,distance):
        """初始化汽车属性"""         #相当于类内部的变量
        self.brand = brand
        self.model = model
        self.year = year
        self.mileage = 0   #新车总里程初始化为0

    def get_main_infomation(self):  #self不能省略
        """获取汽车的主要信息"""
        print("品牌：{} 型号：{} 出厂年份：{}".format(self.brand,self.model,self.year))

    def get_mileage(self):
        """获取总里程数"""
        return "汽车行驶总里程：{}公里".format(self.mileage)
    def set_mileage(self,distance):
        """设置总里程数"""
        self.mileage = distance


print("===============类的继承=================")
#所谓继承，就是底层级的抽象继承高层级的抽象的过程
#简单的继承
#父类

class Car():
    """模拟汽车"""

    def __init__(self,brand,model,year):
        """初始化汽车属性"""         #相当于类内部的变量
        self.brand = brand
        self.model = model
        self.year = year
        self.mileage = 0   #新车总里程初始化为0

    def get_main_infomation(self):  #self不能省略
        """获取汽车的主要信息"""
        print("品牌：{} 型号：{} 出厂年份：{}".format(self.brand,self.model,self.year))

    def get_mileage(self):
        """获取总里程数"""
        return "汽车行驶总里程：{}公里".format(self.mileage)


#构造电动汽车子类  class 子类名（父类名）：
class ElectricCar(Car):
    "模拟电动汽车"

    def __init__(self,brand,model,year):
        """初始化电动汽车属性"""
        super().__init__(brand,model,year) #声明继承父类的属性,此时子类自动继承父类方法

#使用子类
my_electric_car = ElectricCar("NextWeek","FF91",2046)
my_electric_car.get_main_infomation()

#给子类添加属性和方法
class ElectricCar1(Car):
    """模拟电动汽车"""
    #新传入电池容量数据
    def __init__(self,brand,model,year,bettery_size):
        super().__init__(brand,model,year) #声明继承父类的属性,此时子类自动继承父类方法
        self.bettery_size = bettery_size #电池容量
        self.electric_quantity = bettery_size #电池剩余容量
        self.electric2distance_ratio = 5 #电池距离换算系数
        self.remainder_range = self.electric_quantity*self.electric2distance_ratio   #剩余可行驶旅程

    def get_electric_quantity(self):
        """查看当前电池容量"""
        print("当前电池剩余电量:{} kw.h".format(self.electric_quantity))

#使用子类
my_electric_car = ElectricCar1("NewWeek","FF91",2046,70)
my_electric_car.get_electric_quantity()


#重写父类的方法--多态  同样的方法 在不同的子类里面有不同的表达方式
class ElectricCar2(Car):
    """模拟电动汽车"""
    #新传入电池容量数据
    def __init__(self,brand,model,year,bettery_size):
        super().__init__(brand,model,year) #声明继承父类的属性,此时子类自动继承父类方法
        self.bettery_size = bettery_size #电池容量
        self.electric_quantity = bettery_size #电池剩余容量
        self.electric2distance_ratio = 5 #电池距离换算系数
        self.remainder_range = self.electric_quantity*self.electric2distance_ratio   #剩余可行驶旅程

    def get_main_infomation(self):    #重写父类方法
        """获取汽车主要信息"""
        print("品牌：{} 型号：{} 出厂年份：{} 续航里航：{}公里"
              .format(self.brand,self.model,self.year,self.bettery_size*self.electric2distance_ratio))

#调用
my_electric_car = ElectricCar2("NextWeek","FF91",2046,70)
my_electric_car.get_main_infomation()

#用在类中的实例 把电池抽象成一个对象 逻辑更加清晰
class Bettery():
    """模拟电动汽车的电池"""
    def __init__(self,bettery_size = 70):
        self.bettery_size = bettery_size  # 电池容量
        self.electric_quantity = bettery_size  # 电池剩余容量
        self.electric2distance_ratio = 5  # 电池距离换算系数
        self.remainder_range = self.electric_quantity * self.electric2distance_ratio  # 剩余可行驶旅程

    def get_electric_quantity(self):
        """查看当前电池容量"""
        print("当前电池剩余电量:{} kw.h".format(self.electric_quantity))

#重新构造电动汽车类 使模块更加清晰
class ElectricCar4(Car):
    """模拟电动汽车"""

    def __init__(self,brand,model,year,bettery_size):
        """初始化电动汽车属性"""
        super().__init__(brand,model,year) #声明继承父类的属性,此时子类自动继承父类方法
        self.bettery = Bettery(bettery_size) #创建电池实例，并且把这个属性赋值给bettery

    def get_main_infomation(self):
        """获取汽车主要信息"""
        print("品牌：{} 型号：{} 出厂年份：{} 续航里航：{}公里"
              .format(self.brand, self.model, self.year, self.bettery.bettery_size * self.bettery.electric2distance_ratio))
#调用
my_electric_car = ElectricCar2("NextWeek","FF91",2046,70)
my_electric_car.get_main_infomation()

