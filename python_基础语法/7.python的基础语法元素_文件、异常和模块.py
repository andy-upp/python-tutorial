#-*- coding: utf-8 -*-

print("==============文件的读写=============")
#文件的读写
#文件打开  文件打开的通用格式  使用with块的好处是：执行完毕后，自动对文件进行close操作
#with open("文件路径","打开模式",encoding="操作文件的字符编码") as f:
#    """对文件进行相应的读写操作"""

#一个简单的文件读取
with open(r"C:\Projectsrc\Pythonsrc\python-tutorial\python基础语法\test.txt","r",encoding="utf-8") as f:  #第一步 打开文件
    text = f.read()                                                                                       #第二步 读取文件
    print(text)

#程序与文件在同一文件夹，文件路径可简化为文件名
with open(r"test.txt","r",encoding="utf-8") as f:  #第一步 打开文件
    text = f.read()                                #第二步 读取文件
    print(text)

#打开模式
#"r" 只读模式 如果文件不存在 报错
#"w" 覆盖写模式 如文件不存在 则创建；如文件存在，则完全覆盖原文件
#"x" 创建写模式 如文件不存在 则创建；如文件存在 报错
#"a" 追加写模式，如文件不存在，则创建；如文件存在，则在原文件后追加内容
#"b" 二进制文件模式，不能单独使用，需要配合使用如"rb","wb","ab",该模式不需要指定encoding
#"t" 文本文件模式，默认值 需配合使用，如 "rt","wt","at",一般省略，简写成 "r","w","a"
#"+", 与"r","w","x","a"配合使用，在原功能的基础上，增加读写功能
#打开模式缺省 默认为只读模式


#字符编码
#万国码  utf-8  包含全世界所有国家需要用到的字符
#中文编码 gbk 专门解决中文编码问题 windows系统下，如果缺省，则默认为gbk（所在区域的编码）
#为清楚起见 除了处理二进制文件 建议不要缺省 encoding


#文件读取
#逐行进行读取   f.readline
with open(r"test.txt","r",encoding="utf-8") as f:
    text = f.readline()
    print(text)

#读入所有行，以每行为元素形成一个列表 -- f.readlines()
with open(r"test.txt","r",encoding="utf-8") as f:
    text = f.readlines()
    print(text)  #注意每行末尾有换行符

with open(r"test.txt", "r", encoding="utf-8") as f:
        for text in f.readlines(): #遍历列表
          print(text)

#当文件内容过大时，read和readlines占用内存过大，不建议使用，readline用起来不太方便，此时可以直接遍历f
with open(r"test.txt", "r", encoding="utf-8") as f:
    for text in f:                  #f本身就是一个可迭代对象，每次迭代都读取一行内容
        print("直接遍历f：",text)

#图片：二进制文件
with open("test.jpg","rb") as f:
    print(len(f.readlines()))

#文件的写入
#向文件写入一个字符串或字节流（二进制）-- f.write
with open("test.txt","w",encoding="utf-8") as f:
    f.write("你曾经对我说\n")     #文件不存在则创建一个，如果文件存在，新写入的内容会覆盖掉原内容，一定要注意
    f.write("你永远爱着我\n")     #如需要换行，末尾添加换行符 \n
#追加写入
with open("test.txt","a",encoding="utf-8") as f:
    f.write("你曾经对我说\n")    #追加模式
#将一个元素为字符串的列表整体写入文件 -- f.writelines()
ls = ["春天刮着风\n","秋天下着雨\n","春风秋雨多少海誓山盟随风远去\n"]
with open("test.txt","w",encoding="utf-8") as f:
    f.writelines(ls)

#既读又写
#"r+" 如果文件名不存在 则报错，指针在开始，如果要进行写操作，要把指针移动到末尾才能开始写，否则会覆盖前面内容
with open("test1.txt","r+",encoding="utf-8") as f:
#    for line in f:
#        print(line) 全部读一遍后，指针达到结尾
     f.seek(0,2)   #或者可以将指针移动到末尾  f.seek(偏移字节数，位置(0:开始， 1：当前位置， 2：结尾))
     text = ["萧瑟秋风今又是，\n","换了人间。\n"] #将这两句追加到结尾
     f.writelines(text)

#w+ 若文件不存在，则创建 若文件存在，会立即清空原内容
with open("test1.txt","w+",encoding="utf-8") as f:
    text = ["萧瑟秋风今又是，\n","换了人间。\n"]  #清空原内容
    f.writelines(text)   #写入新内容，指针在最后
    f.seek(0,0)  #指针移动到开始
    print(f.read())

#a+ 若文件不存在，则创建，指针在末尾 添加新内容，不会清空原内容
with open("test1.txt","a+",encoding="utf-8") as f:
    text = ["萧瑟秋风今又是，\n","换了人间。\n"]
    f.writelines(text)   #指针在最后，追加新内容
    f.seek(0,0)  #指针移动到开始
    print(f.read())

print("==================数据存储与读取==================")
#本节简单了解两种数据存储结构，csv和json
#csv格式 由逗号将数据分开的字符序列，可以由excel打开
#读取
with open("成绩.csv","r",encoding="utf-8") as f:
    ls = []
    for line in f:      #逐行读取
        ls.append(line.strip("\n").split(",")) #去掉每行的换行符，然后用","进行分割

for res in ls:
    print (res)
#写入
ls = [['编号','数学成绩','语文成绩'],['1','100','90'],['2','96','99']]
with open("score.csv","w",encoding="utf-8") as f:
    for row in ls:
        f.write(",".join(row)+"\n") #用逗号组成字符串形式，末尾加换行符

#json格式 常被用来存储字典类型
#写入 --dump()
import json
scores = {"Peter":{"math":96,"physics":98},"Paul":{"math":96,"physics":98}}
with open("score.json","w",encoding="utf-8") as f:   #写入整个对象
    json.dump(scores,f,indent=4,ensure_ascii=False)  #indent 表示字符换行+缩进

#读取  --load
with open("score.json","r",encoding="utf-8") as f:
    scores = json.load(f)
    for k,v in scores.items():
        print(k,v)

print("================异常处理================")
#常见异常的产生
#除0运算；找不到可读文件 FileNotFoundError；值错误 ValueError 传入一个不期望的值，即使这个值的类型是正确的；索引错误-IndexError，下标超出序列边界；类型错误 -typeError，传入对象类型与要求不符
#当异常发生时，如不预先设定处理方法，程序就会中断,异常的处理  可提高程序的稳定性与可靠性
# try_except  如果try内代码块顺利执行，except不被触发，如果try内代码块发生错误，触发except执行except内代码块
#单分支
x =10
y = 0
try:
    z = x/y
except ZeroDivisionError:    #一般来说会预测到出现什么错误
    print("0不可以被除！")

#多分支
ls = []
d = {"name":"andy"}
try:
    d["age"]
except NameError:
    print("变量名不存在")
except IndexError:
    print("索引超出界限")
except KeyError:
    print("键不存在")

#万能的Exception 所有错误的老祖宗 可以捕捉任何错误类型
ls = []
d = {"name":"andy"}
try:
    print (d["age"])
except Exception:
    print("出错啦")

#用as捕捉异常错误的值
ls = []
d = {"name":"andy"}
try:
    y = m
except Exception as e: #虽然不能获得错误的具体类型，但是可以获得错误的值
    print("错误值：",e)

#try_except_else 如果try模块执行，则else模块也执行，可以将else模块看做是try成功的额外奖赏,如果try内容报错，则else就不会执行
try:
    with open("test1.txt","r",encoding="utf-8") as f:
        text = f.read()
except FileNotFoundError:
    print("找不到该文件")
else:
    print("文件内容：\n",text)

#try_except_finally
#无论try模块是否执行，finally最后都执行
ls = []
d = {"name":"andy"}
try:
    y = x
    print (ls[3])
    print (d["age"])
except Exception as e:
    print (e)
finally:
    print("不论触不触发异常，都将执行")


print("==============模块===============")
#模块分类
#python内置模块 时间库time 随机库random 容器数据类型collection 迭代数据类型 itertools
#第三方库 数据分析numpy pandas 数据可视化 matplotlib 机器学习 scikit-learn 深度学习 tensorflow
#自定义文件 单独的py文件； 
#包--文件夹内有多个py文件，再加上一个__init__.py文件（内容可以为空）

#模块的导入
#导入整个模块：  import 模块名
#调用方式： 模块名.函数名或类名
import time
start = time.time()
time.sleep(3)
end = time.time()
print("程序运行用时：{:.2f}秒".format(end-start))
#从模块中导入类或函数 - from 模块 import 类名或函数名
#调用方式：函数名或类名
from itertools import product
ls = list(product("AB","123"))
print (ls)
#一次导入多个 from function import fun1,fun2
#导入模块中所有的类和函数  from 模块 import *
#调用方式：函数名或类名
from random import *
print(randint(1,100))
print(random())
#模块的搜索查找路径
#首先搜索内存中已经加载的模块，其次导入内置模块，最后导入sys.path路径中包含的模块
# sys.path的第一个路径是当前执行文件所在的文件夹，若所需模块不在其中，则需要将模块的路径添加到sys.path
#import sys
#sys.path.append("C:\\xxx\\xxx\\xxx") #注意是双斜杠



























































































































































