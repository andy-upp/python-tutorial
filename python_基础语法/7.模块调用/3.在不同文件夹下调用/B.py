import sys
sys.path.append(r'/Users/liguanghui/Documents/Projectsrc/PythonSrc/python-tutorial/python_基础语法/7.模块调用/2.调用类')
##python import模块时， 是在sys.path里按顺序查找的。sys.path是一个列表，里面以字符串的形式存储了许多路径。使用A.py文件中的函数需要先将他的文件路径放到sys.path中
import A
a = A.A(2,3)
a.add()
