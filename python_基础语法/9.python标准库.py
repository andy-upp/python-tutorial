#-*- coding: utf-8 -*-

print("================time标准库================")
#time库 python处理时间的标准库
#获取现在时间  time.localtime()
#获取UTC世界统一时间 time.gmtime   北京时间比统一时间早8个小时
import time
t_local = time.localtime()
t_UTC = time.gmtime()
print("t_local",t_local) #本地时间
print("t_UTC",t_UTC) #UTC统一时间
print(time.ctime()) #返回本地时间的字符串

#时间戳与计时器
#time.time() 返回自纪元以来的秒数，记录sleep
#time.perf_counter() 随意选取一个时间点，记录现在时间到该时间点间隔秒数，记录 sleep
#time.process_time() 随意选取一个时间点，记录现在时间到该时间点间隔秒数，不记录 sleep
#perf_counter()精度比time()更高一些
t_1_start = time.time()
t_2_start = time.perf_counter()
t_3_start = time.process_time()
print(t_1_start)
print(t_2_start)
print(t_3_start)
#时间格式化输出 time.triftime 自定义格式化输出
lctime = time.localtime()
print(time.strftime("%Y-%m-%d %A %H:%M:%S",lctime))
#睡眠
time.sleep(1)

print("================random标准库==============")
#python通过random库提供各种伪随机数
#随机种子 -- seed（a=None）
#相同种子会产生相同随机数，如果不设置随机种子，以系统当前时间为默认值
from random import *
seed(10)
print(random())
seed(10)
print(random())
#产生随机整数
#randint(a,b) -- 产生[a,b]之间的随机整数
numbers = [randint(1,10) for i in range(10)]
print(numbers)
#randrange(a) -- 产生[0,a)之间的随机整数,不包含a
numbers = [randrange(10) for i in range(10)]
print(numbers)
#randrange(a,b,step) -- 产生[a,b)之间以step为步长的随机整数
numbers = [randrange(0,10,2) for i in range(10)]
print(numbers)
#产生随机浮点数
#random()--产生[0.0,1.0]之间的随机浮点数
numbers = [random() for i in range(10)]
print(numbers)
#uniform(a,b)-- 产生[a,b]之间的随机浮点数
numbers = [uniform(2.1,3.5) for i in range(10)]
print(numbers)
#序列用函数
#choice(seq)--从序列类型中随机返回一个元素
print(choice(['win','lose','draw']))
print("python")
#choices(seq,weights=None,k) --对序列类型进行k次重复采样，可设置权重
#重复采样5次,取完之后放回再取
print(choices(['win','lose','draw'],k=5))
#可对每个元素设置权重
print(choices(['win','lose','draw'],[4,4,2],k=10))
#shuffle(seq) 将序列类型中元素随机排列，返回打乱后对序列
numbers = ["one","two","three","four"]
shuffle(numbers)
print(numbers)
#sample(pop,k) -- 从pop类型中随机取k个元素，以列表类型返回
print(sample([10,20,30,40,50],k=3))
#概率分布  以高斯分布为例
#gauss(mean,std) 生产一个符合高斯分布的随机数 mean是均值 std是标准差
number = gauss(0,1)
print(number)
#多生成几个画直方图显示
import matplotlib.pyplot as plt
res = [gauss(0,1) for i in range(100000)]
plt.hist(res,bins=1000)
#plt.show()
print("================collections标准库=========")
#提供容器数据类型，可以看作之前学习过的组合数据类型的扩展
#namedtuple，具名元组，即有名字的元组
#构建一个新的元组子类，定义方法如下：typename是元组名字，field_names是域名, 即元组里面要存的元素的名字
import collections
#collections.namedtuple(typename,field_names,*,rename=False,defaults=None,module=None)
Point = collections.namedtuple("Point",["x","y"])
#对Point对域名赋值，完成初始化操作
p = Point(1,y=2)
print(p)
#p作为一个元组，具有元组的属性，可以调用属性，解包赋值
print(p.x)
print(p.y)
#有元组的性质
print(p[0])
print(p[1])
x,y = p
print(x)
print(y)
print(isinstance(p,tuple))
#Counter  计数器工具
from collections import Counter
s = "牛奶奶找刘奶奶买牛奶"
colors = ['red','blue','red','green','blue','blue']
cnt_str = Counter(s)
cnt_color = Counter(colors)
print(cnt_str)
print(cnt_color)
#Counter是字典类型的子类
print(isinstance(Counter(),dict))
#最常见的统计 -- most_commom(n) 提供n个频率最高的元组和计数
#比如打印出cnt_color中2个出现频率最高的元素和计数
print(cnt_color.most_common(2))
#将已经统计好的元素展开 -- elements()
print(list(cnt_str.elements()))
#其他一些加减操作
c = Counter(a=3,b=1)
d = Counter(a=1,b=2)
print(c+d)
#双向队列 可以方便的在队列两边高效、快速的增加和删除元素
from collections import deque
d = deque('cde')
print(d)
d.append("f") #右端增加
d.append("g")
d.appendleft("b")#左端增加
d.appendleft("g")
print(d)
d.pop()  #右端删除
d.popleft()#左端删除
print(d)

print("================itertools标准库===========")
#itertools标准库中有很多产生迭代器的方法
#排列组合迭代器
#product  -- 笛卡尔积
import itertools
for i in itertools.product('ABC','01'):
    print(i)
for i in itertools.product('ABC',repeat = 3):
    print (i)
#permutations  排列
for i in itertools.permutations('ABCD',3):  #从ABCD中取3个进行排列，3是排列的长度
    print(i)
for i in itertools.permutations(range(3)):
    print(i)

#combinations 组合
for i in itertools.combinations('ABCD',2): #2是组合的长度
    print(i)
for i in itertools.combinations(range(4),3):
    print(i)
#combinations_with_replacement -- 元素可重复组合
for i in itertools.combinations_with_replacement('ABC',2): #2是组合的长度
    print(i)
for i in itertools.product('ABC',repeat=2):
    print(i)

#拉链
#zip -- 短拉链
for i in zip("ABC","012","xyz"):
    print(i)
#长度不一致时，执行到最短的对象处，就停止
for i in zip("ABC","012345"):
    print(i)
#长度不一致时，执行到最长对象处，就停止，缺省的元素用None或指定字符替代
for i in itertools.zip_longest("ABC","012345"):
    print(i)
for i in itertools.zip_longest("ABC","012345",fillvalue="?"):
    print(i)
#无穷迭代器
#count(start=0,step=1) 计数
#创建一个迭代器 它从start值开始，返回均匀间隔的值
print(itertools.count(10))
#cycle  循环
#创建一个迭代器，返回iterable中所有元素，无限重复
print(itertools.cycle("ABC"))
#repeat(object[,times]) --重复
#其他迭代器
#chain(iterables)  锁链 把一组迭代对象串联起来 形成一个更大的迭代器
#enumerate(iterable,start=0) 枚举 python内置
#groupby(iterable,key=None) 分组
