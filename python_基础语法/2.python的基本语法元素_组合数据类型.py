#-*- coding: utf-8 -*-

#组合数字类型 列表 元组 字典 集合
print("============列表============")
#2.1 列表  序列类型  数据有位置顺序 能够通过位置序号访问其中元素
#列表是一个可以使用多种类型元素，支持元素都增，删，改，查操作都序列类型
a = ["Python",1989,True,{"version":3.7}]
print (a)
print (a[3])
#列表的另一种产生方式：list(可迭代对象)
#可迭代对象包括：字符串、元组、集合、range()
#字符串转列表
print(list("abcdefg"))
#元组转列表
print (list(("we","are","one","family")))
#集合转列表
print (list({"lilei","hanmeimei","jim","Green"}))
#特殊的range()
for i in  [0,1,2,3,4,5]:
    print (i)

for i in range(6):
    print (i)

#range(起始数字，中止数字，数字间隔)
#如果起始数字缺省，默认为0，不包含中止数字，数字间隔缺省，默认为1
print("sdfasddfassdf99999999999999999999")
for i in range(1,11):
    print(i)
print("sdfsdfsdfsddfsddfsddfsfsdfsdfsdf")


#range转列表
print(list(range(1,11,2)))
#列表的性质
#列表的长度  len(列表)
ls = [1,2,3,4,5]
print (len(ls))
#列表的索引，与同为序列类型的字符串完全相同
#变量名[位置编号]，正向索引从0开始，方向索引从-1开始
cars = ["BYD","BMW","AUDI","TOYOTA"]
print(cars[0])
print (cars[-1])
#列表的切片
#变量名[开始位置：结束位置：切片间隔]
#正向切片
print (cars[:3])
print (cars[1:4:2])
print (cars[:])
print (cars[-4:-2])
#反向切片
print(cars[:-4:-1]) #开始位置缺省，默认为-1
print(cars[::-1]) #获得反向列表
#列表的操作符
#用list1+list2 的形式实现列表的拼接
a = [1,2]
b = [3,4]
print (a+b)
#用n*list或者list*n的方式实现列表的成倍复制
#列表初始化的一种方式
print ([0]*10)
#列表的操作方法
#增加元素  在末尾增加元素  列表.append(待增加元素)
languages = ["Pyhton","C++","R"]
languages.append("Java")
print (languages)
#在任意位置插入元素  列表.insert(位置编号，待增元素)，在位置编号相应元素前插入待增元素
languages.insert(1,"C")
print (languages)

#在末尾整体插入另一列表
# 列表1.extend(列表2)，extend将列表2的元素逐个添加到列表1中
languages = ['Pyhton','C','C++','R','Java']
languages.extend(['Ruby','PHP'])
print (languages)
# append将列表2整体作为一个元素添加到列表1中
languages.append(['Ruby','PHP'])
print (languages)
#删除元素 删除列表i位置的元素 列表.pop(位置)
languages =  ["Python","C++","C","R","Java"]
languages.pop(1)
print (languages)
#不写位置信息，默认删除最后一个元素
languages.pop()
print (languages)
#删除列表中的第一次出现的待删除元素， 列表.remove(待删元素)
languages = ['Python','C','R','Java']
languages.remove('C')
print (languages)
#查找元素  列表中第一次出现待查找元素待位置  列表.index(待查元素)
languages = ['Python','C','R','Java']
idx = languages.index("Python")
print (idx)
#修改元素  通过"先索引后赋值"的方式，对元素进行修改 列表名[位置]=新值
languages = ['Python','C','R','Java']
languages[1] = "C++"
print (languages)
#列表复制
#方法1： 列表.copy()
languages = ['Python','C','R','Java']
# languages_2 = languages.copy()
# languages.pop()
# print (languages)
# print (languages_2)

#方法2：列表[:]
languages = ['Python','C','R','Java']
languages_3 = languages[:]
languages.pop()
print (languages)
print (languages_3)

#列表的排序
#使用 列表.sort()对列表进行永久排序 直接在列表上进行操作，无返回值
ls = [2,5,2,8,19,3,7]
ls.sort()
print (ls)
#递减排序
ls1 = [2,5,2,8,19,3,7]
ls1.sort(reverse=True)
print (ls1)
#使用sorted(列表）对列表进行临时排序，原列表保持不变，返回排序后的列表
ls2 = [2,5,2,8,19,3,7]
ls_2 = sorted(ls2)
print(ls_2)
#列表的翻转 使用 列表.reverse() 对列表进行永久翻转 直接在列表上进行操作，无返回值
ls3 = [1,2,3,4,5]
ls3.reverse()
print (ls3)
#使用for循环对列表进行遍历
ls4 = [1,2,3,4,5]
for i in ls4:
    print (i)

print ("===============元组类型=================")
#元组 序列顺序 但是不支持修改，增加，减少 即不可变带列表
#其他操作与列表操作完全一致
b = (1,2,3,4,5)
print (b[2])

#打包与解包
def f1(x):
    return x**2,x**3  #实现打包返回，即将结果作为一个元组返回
print (f1(3))
print (type(f1(3)))
#解包处理
a,b = f1(3)
print (a)
print (b)
#解包例子2
numbers = [201901,201902,201903]
name = ["xiaoming","xiaohong","xiaoqiang"]
#将列表numbers和name用zip进行打包之后，就将各自相同位置的元素变成一个元组
print(list(zip(numbers,name)))
#每次取到一个元素，立即进行解包赋值
for number,name in zip(numbers,name):
    print(number,name)

print ("================字典类型=================")
#字典 映射类型，通过键-值带映射实现数据的存储和查找，内部是无序的，字典的键不能重复
student = {201901:"小明",201902:"小红",201903:"小强"}
#字典的索引 通过 字典[键]的形式来获取对应的值
print (student[201901])
student = {}
#字典的键必须是不可变类型，如果键变了，就找不到对应的存储的值了
#不可变类型： 数字、字符串、元组。一旦确定，它就是它自己，变了就不是它了，这些可以作为字典的键
#可变类型：列表、字典、集合。 一旦确定，还可以随意增删改

#字典的长度--键值对的个数
student1 = {201901:"小明",201902:"小红",201903:"小强"}
print (len(student1))
#字典的操作方法
#增加键值对 通过变量名[新键] = 新值
student2 = {201901:"小明",201902:"小红",201903:"小强"}
student2[201904] = "小雪"
print (student2)
#删除键值对
#通过del 变量名[待删除键] 删除键值对
student3 = {201901:"小明",201902:"小红",201903:"小强"}
del student3[201903]
print (student3)
#通过变量名.pop(待删除键) 删除，同时获得待删除键值对对值
student4 = {201901:"小明",201902:"小红",201903:"小强"}
value = student4.pop(201903)
print (value)
print (student4)
#变量名.popitem()随机删除一个键值对，并以元组返回删除对键值对
student5 = {201901:"小明",201902:"小红",201903:"小强"}
key,value = student5.popitem()
print(key,value)
print (student5)
#修改键值对对值 通过先索引后赋值对方法对相应对值进行修改
student6 = {201901:"小明",201902:"小红",201903:"小强"}
student6[201902] = "小雪"
print (student6[201902])
#d.get()方法 　d.get(key,default) 从字典d中获取键key对应对值，如果没有这个值，则返回default
#统计 "牛奶奶找刘奶奶买牛奶"中字符出现对频率
s = "牛奶奶找刘奶奶买牛奶"
d = {}
for i in s:
    d[i] = d.get(i,0) + 1
print (d)
#d.keys() 取字典里所有的键
#d.values()  取字典里所有的值
student6 = {201901:"xiaoming",201902:"xiaohong",201903:"xiaoqiang"}
print (list(student6.keys()))
print (list(student6.values()))
#d.items()方法及字典的遍历
print(list(student6.items()))
for k,v in student6.items():
    print (k,v)

print("=================集合=================")
#集合 一系列互不相等元素的集合，无序且互斥
#集合元素必须是不可变类型：数字，字符串或元组，可视作字典的键
#集合可以看作是没有值，或者值为None的字典
s = {"xiaoming","xiaohong","xiaoqiang","xiaoming"} #可用于去重
print (s)
#集合的运算
#例子 通过集合进行交集并集的运算
Chinese_A = {"liu","zhang","manyu","zhong","gu","lin"}
print (Chinese_A)
Math_B = {"lin","guo","wang","liu","manyu","li"}
print (Math_B)
#交集运算，返回一个新集合，包括同时在A和B中的元素
print  (Chinese_A&Math_B)
#并集运算，返回一个包含A和B所有元素的新集合
print (Chinese_A|Math_B)
#返回包含A和B非共同元素的新集合
print (Chinese_A^Math_B)
#返回一个包含在集合A中但不在集合B中的元素的集合
print (Chinese_A - Math_B)
#集合的操作方法
#增加元素
stars = {"liu","zhang","manyu"}
stars.add("wang")
print (stars)
#移除元素
stars.remove("wang")
#计算集合的长度
print (len(stars))
#集合的遍历
for star in stars:
    print(star)






